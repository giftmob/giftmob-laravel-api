<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        User::truncate();
        DB::table('role_user')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $userRole = Role::where('name', '=', 'user')->first();
        $adminRole = Role::where('name', '=', 'admin')->first();

        $admin = User::create([
            'first_name' => 'Rory',
            'last_name' => 'Jarrard',
            'email' => 'rory@non.com',
            'password' => Hash::make('silver'),
        ]);

        $user1 = User::create([
            'first_name' => 'Admin',
            'last_name' => 'Jones',
            'email' => 'admin@non.com',
            'password' => Hash::make('silver'),
        ]);

        $user2 = User::create([
            'first_name' => 'User',
            'last_name' => 'Smith',
            'email' => 'user@non.com',
            'password' => Hash::make('silver'),
        ]);

        $admin->roles()->attach($adminRole);
        $user1->roles()->attach($userRole);
        $user2->roles()->attach($userRole);
    }
}
