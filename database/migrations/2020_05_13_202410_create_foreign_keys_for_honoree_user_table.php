<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForeignKeysForHonoreeUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('honoree_user', function (Blueprint $table) {
            $table->foreign('honoree_id')->references('id')->on('honorees')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('honoree_user', function (Blueprint $table) {
            $table->dropForeign('honoree_user_honoree_id_foreign');
            $table->dropForeign('honoree_user_user_id_foreign');
        });
    }
}
