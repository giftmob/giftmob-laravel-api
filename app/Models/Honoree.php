<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Honoree extends Model
{
    protected $fillable = ['first_name', 'last_name', 'birthday', 'gender'];

    public function users() {
        return $this->belongsToMany('App\Models\User');
    }
}
