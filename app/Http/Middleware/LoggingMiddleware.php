<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Logger;

class LoggingMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Logger::configure(app()->basePath() . '/config/log4php/config.' . env('APP_ENV') . '.xml');

        return $next($request);
    }
}

