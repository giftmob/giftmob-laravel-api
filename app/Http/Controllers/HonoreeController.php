<?php

namespace App\Http\Controllers;

use App\Models\Honoree;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class HonoreeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $fields = request()->validate([
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'birthday' => 'date',
            'gender' => 'in:unspecified,male,female'
        ]);

        // TODO: this should be more robust
        foreach (auth()->user()->honorees as $h) {
            if ($h->first_name == $fields['first_name'] && $h->last_name == $fields['last_name']) {
                return response()->json(['message'=>'You already have a relation with this name'], 401);
            }
        }

        try {
            $honoree = Honoree::create($fields);

            $honoree->users()->attach(auth()->user());
            auth()->user()->honorees()->attach($honoree);
        } catch (Exception $e) {
            $code = $e->getCode() >= 100 && $e->getCode() < 600 ? $e->getCode() : 500;
            return response()->json(['message' => 'could not create honoree: ' . $e->getMessage()], $code);
        }

        return response()->json(['message' => "honoree {$honoree->id} created"], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Honoree  $honoree
     * @return Response
     */
    public function show(Honoree $honoree)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Honoree  $honoree
     * @return Response
     */
    public function edit(Honoree $honoree)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Honoree  $honoree
     * @return Response
     */
    public function update(Request $request, Honoree $honoree)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Honoree  $honoree
     * @return Response
     */
    public function destroy(Honoree $honoree)
    {
        //
    }
}
