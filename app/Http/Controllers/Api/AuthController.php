<?php

namespace App\Http\Controllers\Api;

use App\Models\Role;
use App\Models\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;
use Logger;

class AuthController extends Controller
{
    private $logger;

    public function __construct()
    {
        $this->logger = Logger::getRootLogger();
    }

    /**
     * @return JsonResponse
     */
    public function login()
    {
        $login = request()->validate([
            'email' => 'required|email',
            'password' => 'required|string'
        ]);

        $this->logger->info("Logging in: " . json_encode($login));

        if (!auth()->attempt($login)) {
            return response()->json(['message' => 'incorrect credentials'], 401);
        }

        $token = JWTAuth::fromUser(auth()->user());

        return response()->json(['user' => auth()->user()->only(['email', 'first_name', 'last_name']), 'token' => $token], 200);
    }

    /**
     * Refresh access token for user
     * @return JsonResponse
     */
    public function refresh()
    {
        $newToken = auth()->refresh();
        return response()->json(['token'=>$newToken], 201);
    }

    /**
     * @return JsonResponse
     */
    public function register()
    {

        $fields = request()->validate([
            'email' => 'required|email',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'password' => 'required|string'
        ]);

        $fields['password'] = Hash::make($fields['password']);

        try {
            $user = User::create($fields);

            $userRole = Role::where('name', '=', 'user')->first();
            $user->roles()->attach($userRole);
        } catch (Exception $e) {
            $eCode = $e->getCode();
            $statusCode = $eCode < 600 ? $eCode : 500;

            $message = $eCode == 1062 || $eCode == 23000 ? 'Email already taken' : 'Could not create user';

            return response()->json(['message' => $message], $statusCode);
        }

        $token = JWTAuth::fromUser($user);

        return response()->json(compact('user', 'token'), 200);
    }
}
